# _¡Hola! Bienvenido a mi desafío._

## INTRODUCCIÓN

A continuación, se enunciarán las consideraciones y observaciones que se deben de tener en cuenta para el correcto uso del código aquí a disposición.

Este servidor backend cumple con los 3 (tres) niveles del desafío expuesto por [Andres Szwarcfiter](andres.szwarcfiter@teamknowlogy.com), por lo tanto, todos los requerimientos expuestos en su correo se encuentran funcionando en Amazon Web Services.

## CONFIGURACIONES

Para lograr cumplir con todos los requerimientos mencionados se ha utilizado los siguientes programas con sus respectivas versiones:
- **[NodeJs](https://nodejs.org/dist/v12.19.0/docs/api/) - Versión: 12.19.0**
- **[Npm](https://nodejs.org/dist/v12.19.0/docs/api/) - Versión: 6.14.8**
- **[MongoDB Community Edition](https://docs.mongodb.com/upcoming/release-notes/5.0/) - Versión: 5.0.3**
- **_(Opcional)_ [Visual Studio Code](https://code.visualstudio.com/download) - Versión: 1.61.2**

## CORRER DE MANERA REMOTA

El servidor de DNA se encuentra funcionando en todo momento en los servidores remotos de AWS. Por lo tanto, basta con acceder a:

`http://3.21.165.201/`

**Advertencia:** puede ocurrir que la primer petición al servidor demore un poco más de lo usual. Esto puede ocurrirse dado que la máquina virtual del servicio de EC2 de AWS al no recibir peticiones por un determinado tiempo entra en modo de descanso. Al recibir una nueva petición, ésta despierta y lanza todos los procesos a correr nuevamente.

## CORRER DE MANERA LOCAL

Si usted desea correr este proyecto de manera local, por favor, siga los siguientes pasos a continuación:

### PASO 0: INSTALACIÓN Y CONFIGURACIÓN
Para realizar los pasos siguientes es recomandable que ya posea instalado los software necesarios expuestos en el apartado de "CONFIGURACIONES".

### PASO 1: CLONAR REPOSITORIO
Usted debe clonar este repositorio desde el enlace ofrecido mediante correo electrónico.

`git clone https://gitlab.com/Raviolero/adn.git`

### PASO 2: INSTALAR DEPENDENCIAS
En este proyecto se ha establecido al sistema de Git que ignore las carpetas de "node_modules" y "dist".
Por ende, es necesario que reinstalar los modulos necesarios.

`npm install`

### PASO 3: EJECUTAR EL SERVICIO DE LA BASE DE DATOS
Antes de poder correr el servidor es necesario asegurarse que el servicio de la base de datos de MongoDB esté corriendo. Para ello, dependiendo del sistema operativo que usted esté utilizando ejecutará el siguiente código para verificar que el servicio esté arriba.

*Windows* -> `mongod`

*Linux Ubuntu* -> `sudo systemctl start mongod`

**Nota: en caso de no reconocer el comando en Windows, por favor, revise que la carpeta "bin" de MongoDB esté dentro de las rutas de las variables de entorno de Windows.**

### PASO 4: CORRER EL SERVIDOR
Ahora es momento de ejecutar nuestro proyecto de backend. Proceda a ejecutar el siguiente comando:

`npm run dev`

### PASO 5: PROBAR EL SERVIDOR
Para realizar las pruebas de manera local, se ha facilitado un archivo de extensión *".http"* llamado *"request.http"*. Este archivo posee peticiones HTTP pre diseñadas para ahorrar tiempo y esfuerzo al QA Tester.

**Nota:** no se ha establecido la extensión del archivo *request* a *.md* dado que Visual Studio Code, junto a la extensión de [Rest Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client), reconoce únicamente las extenciones *.http* para realizar peticiones HTTP mediante el escritor de código. Esto permite al desarrollador prescindir de un software adicional (Postman, Insomnia, entre otros) para realizar dichas peticiones.

## AGRADECIMIENTOS

Quiero dar un apartado especial para brindar mis más sinceros agradecimientos al equipo de Team Knowlogy por brindar un tiempo a evaluar mi proyecto y tomarse el tiempo de charlar conmigo.

## CONTACTO

- **Correo electrónico: rami_eduard@hotmail.com**
- **GitLab: @Raviolero**