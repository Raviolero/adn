import {Schema, model, Document} from 'mongoose'

const dnaSchema = new Schema({
    dna: [],
    hasMutation: Boolean
})

interface Idna extends Document{
    dna: string[];
    hasMutation: boolean;
}

export default model<Idna>('dna', dnaSchema)