import mongoose from 'mongoose';

let URI: string = "mongodb://localhost/dna"

export async function startConnection(){
    await mongoose.connect(URI);

    console.log('Database is connected');  
}