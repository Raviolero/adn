import express from 'express';
import morgan from 'morgan';
import indexRoutes from './routes/index.routes'

const app = express();

// settings
app.set('port', process.env.PORT || 3000);
app.use(express.json());

// middlewares
app.use(morgan('dev'));

// routes
app.use('/', indexRoutes);

export default app;