import { Router } from "express";
import { mutation } from "../controllers/dna.controller"
import { statsRequest } from "../controllers/stats.controller"

const router = Router();

router.route('/mutation').post(mutation);

router.route('/stats').get(statsRequest);

export default router;