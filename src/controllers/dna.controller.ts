import { Request, Response } from "express";
import dnaMongoose from "../models/dna";

export function mutation(req: Request, res: Response) {
  return res.send(
    "Mutation of the array presented is: " + hasMutation(req.body.dna)
  );
}

/**
 * Coordinating function.
 * @param dna
 * @returns Returns true in case of having some type of mutation. Otherwise, it will return false.
 */
export function hasMutation(dna: string[]): boolean {
  let hasMutation = false;
  let accMutations = 0;

  if (hasValidElement(dna) && validateDimensionality(dna)) {
    const mutationHorizontal = hasMutationHorizontal(dna);
    const mutationVertical = hasMutationVertical(dna);
    const mutationDiagonalMain = hasMutationDiagonal(dna);
    const mutationDiagonalReverse = hasMutationReverseDiagonal(dna);

    console.log("Mutacion horizontal: " + mutationHorizontal);

    console.log("Mutacion vertical: " + mutationVertical);

    console.log("Mutacion diagonal principal: " + mutationDiagonalMain);

    console.log("Mutacion diagonal inversa: " + mutationDiagonalReverse);

    if(mutationHorizontal + mutationVertical + mutationDiagonalMain + mutationDiagonalReverse >= 2){
      hasMutation = true;
    }
  }

  // //Database
  saveDna(dna, hasMutation);

  return hasMutation;
}

async function saveDna(dna: string[], hasMutation: boolean) {
  const newDna = {
    dna: dna,
    hasMutation: hasMutation,
  };

  const dnaDb = new dnaMongoose(newDna);
  console.log(dnaDb);

  await dnaDb.save();
}

/**
 * Check if a DNA array has a horizontal mutation.
 * @param dna
 * @returns If it has a horizontal mutation, it will return true. Otherwise, it will return false.
 */
function hasMutationHorizontal(dna: string[]): number {
  let accMutation = 0;

  dna.forEach((e: string) => {
    let chartPrev;
    let chartCurrent;
    var acc = 0;

    for (let i = 0; i <= e.length; i++) {
      chartPrev = e.charAt(i);
      chartCurrent = e.charAt(i + 1);

      if (chartCurrent == chartPrev) {
        acc++;

        if (acc == 3) {
          accMutation++;
        }
      } else {
        acc = 0;
      }
    }
  });

  return accMutation;
}

/**
 * Check if a DNA array has a vertical mutation.
 * @param dna
 * @returns If it has a vertical mutation, it will return true. Otherwise, it will return false.
 */
function hasMutationVertical(dna: string[]): number {
  let accMutation = 0;
  let arrayReverse: string[] = [];

  for (let i = 0; i < dna.length; i++) {
    let stringAux: string = "";

    dna.forEach((e) => {
      stringAux = stringAux.concat(e.charAt(i));
    });

    arrayReverse.push(stringAux);
  }

  accMutation = hasMutationHorizontal(arrayReverse);

  return accMutation;
}

/**
 * Check if a DNA array has a diagonal mutation.
 * @param dna
 * @returns If it has a diagonal mutation, it will return true. Otherwise, it will return false.
 */
function hasMutationDiagonal(dna: string[]): number {
  let accMutation = 0;
  let arrayReverse: string[] = [];

  for (let i = 0; i < dna.length; i++) {
    let stringAux: string = "";

    dna.forEach((e) => {
      stringAux = stringAux.concat(e.charAt(i));
      i++;
    });

    arrayReverse.push(stringAux);
  }

  accMutation = hasMutationHorizontal(arrayReverse);

  return accMutation;
}

/**
 * Check if a DNA array has a reverse diagonal mutation.
 * @param dna
 * @returns If it has a reverse diagonal mutation, it will return true. Otherwise, it will return false.
 */
function hasMutationReverseDiagonal(dna: string[]): number {
  let accMutation = 0;
  let arrayReverse: string[] = [];

  for (let i = dna.length - 1; i > 0; i--) {
    let stringAux: string = "";

    dna.forEach((e) => {
      stringAux = stringAux.concat(e.charAt(i));
      i--;
    });

    arrayReverse.push(stringAux);
  }

  accMutation = hasMutationHorizontal(arrayReverse);

  return accMutation;
}

/**
 * Verify that the dimensionality of the array provided by the user is of order NxN.
 * @param dna
 * @returns Returns true in case the dimensionality is NxN. Otherwise, it will throw an exception.
 */
function validateDimensionality(dna: string[]): boolean {
  dna.forEach((e) => {
    if (e.length !== dna.length) {
      throw Error(
        "The matrix entered does not respect the order NxN. The item '" +
          e +
          "' caused this error."
      );
    }
  });

  return true;
}

/**
 * Verify that the sequence of characters are valid
 * @param dna String array to be evaluated
 * @returns Returns true if it contains valid characters. Otherwise, it will throw an exception
 */
function hasValidElement(dna: string[]): boolean {
  let chartValids = [/A/g, /T/g, /C/g, /G/g];

  dna.forEach((e: string) => {
    chartValids.forEach((aChartValid) => {
      e = e.replace(aChartValid, "");
    });

    if (e.length > 0) {
      throw new Error(
        "An error has been found. The following invalid characters have been detected: " +
          e
      );
    }
  });

  return true;
}
