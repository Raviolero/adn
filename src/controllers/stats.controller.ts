import { Request, Response } from "express";
import dnaMongoose from "../models/dna";

export function statsRequest(req: Request, res: Response) {
  stats().then((iStats) => {
    res.send(
      "The results are => Count Mutation: " +
        iStats.count_mutations +
        ", Count No Mutation: " +
        iStats.count_no_mutations +
        " and the Ratio: " +
        iStats.ratio
    );
  });
}

/**
 * Calculate the necessary stats.
 * @returns Returns an interface object "IStats" which has the statistics of: count_mutations, count_no_mutations and the ratio.
 */
async function stats(): Promise<IStats> {
  const count_mutations = await dnaMongoose.find({ hasMutation: true });
  const count_no_mutations = await dnaMongoose.find({ hasMutation: false });
  const ratio = count_mutations.length / count_no_mutations.length;

  const stats: IStats = {
    count_mutations: count_mutations.length,
    count_no_mutations: count_no_mutations.length,
    ratio: ratio,
  };

  return stats;
}

interface IStats {
  count_mutations: number;
  count_no_mutations: number;
  ratio: number;
}
